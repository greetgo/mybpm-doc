# MyBPM

<hr>
<br/>

## Основные факты о MyBPM

<br/>

## Как начать строить свою систему?

- [Настройте свои бизнес-объекты](020_forms_object.md)
- [Создайте свою структуры компании](010_for_users/040_company)
- [Постройте свою логику не прибегая помощи разработчиков](030_businessProcess_script)
- [Описание возможностей](010_for_users/description_features.md)
- [Конфигурации](010_for_users/configuration.md)
- [API MyBPM](040_for_developers/050_APIService.md)




<br/>

## Перейти

[Пользователи](010_for_users.md)

[Формы и объекты](020_forms_object.md)

[Бизнес-процесс и скрипты](030_businessProcess_script/businessProcess.md)

[Разработчикам](040_for_developers/040_intergrationDB.md)










