#!/usr/bin/env bash

set -e

for host in master1 master2 master3 worker4 worker5 worker6 ; do
  ssh "$host" sudo mkdir -p /data/mybpm/logs
  ssh "$host" sudo chown -Rv 1000:1000 /data/mybpm
done
