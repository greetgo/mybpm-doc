#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 113

NS=mybpm-platform-test

kubectl create secret generic -n "$NS" out-mybpm-kz \
  --from-file=.dockerconfigjson=./05-auth-out-mybpm-kz.json \
  --type=kubernetes.io/dockerconfigjson
