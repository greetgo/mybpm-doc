#!/usr/bin/env bash

set -e

kk=kubectl

function setup_node() {
  node=$1
  shift
  for label in $* ; do
    #echo node ${node} ${label}
    if [[ ${label} == +* ]] ; then
      label=${label:1}
      #echo PLUS ${label}
      ${kk} label --overwrite nodes ${node} ${label}=ok
    fi
    if [[ ${label} == -* ]] ; then
      label=${label:1}
      #echo MINUS ${label}
      ${kk} label --overwrite nodes ${node} ${label}-
    fi
  done

}

setup_node master1 +api -web
setup_node master2 +api -web
setup_node master3 -api -web
setup_node worker4 -api -web
setup_node worker5 -api +web
setup_node worker6 -api +web
