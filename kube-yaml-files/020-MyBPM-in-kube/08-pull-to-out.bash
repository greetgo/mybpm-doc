#!/usr/bin/env bash

set -e

function pull_push() {
  image1=$1
  image2=$2
  docker pull "$image1"
  docker tag "$image1" "$image2"
  docker push "$image2"
}

pull_push dockerhub.mybpm.kz/mybpm-api-dev:4.5.1215  out.mybpm.kz/mybpm-api-dev:4.5.1215
pull_push dockerhub.mybpm.kz/mybpm-web-dev:4.5.1111  out.mybpm.kz/mybpm-web-dev:4.5.1111
