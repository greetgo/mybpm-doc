# Fedora 35: установка docker

Взято из: https://docs.docker.com/engine/install/fedora/

Вначале зачищаем предварительные установки:

    sudo dnf remove -y              \
                  docker             \
                  docker-client       \
                  docker-client-latest \
                  docker-common         \
                  docker-latest          \
                  docker-latest-logrotate \
                  docker-logrotate         \
                  docker-selinux            \
                  docker-engine-selinux      \
                  docker-engine

Теперь подготавливаем репозиторий:

```bash
sudo dnf -y install dnf-plugins-core

sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
```

И устанавливаем докер:

```bash
sudo dnf install -y docker-ce docker-ce-cli containerd.io
```

И активируем его:

```bash
sudo systemctl enable docker
sudo systemctl start  docker
```

Далее прописываем группу:

```bash
sudo usermod -aG docker $USER
```

Проверяем, что он работает:
```bash
docker run --rm hello-world
```
Должен появиться текст ```Hello from Docker! .....```

Далее необходимо [настроить докер](001-02-linux-docker-configure.md)
