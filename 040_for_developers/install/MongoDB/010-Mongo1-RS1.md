# 1) Установка монги по топологии Mongo1-RS1

В этом руководстве кластер MongoDB будет развёртываться на серверах: master1 master2 master3 - в вашем случае это
могут быть другие сервера.

Предполагается, что на серверах уже есть директория dist в домашней директории пользователя. Если нет, то создайте:

    ssh master1 mkdir dist
    ssh master2 mkdir dist
    ssh master3 mkdir dist

## 1.1) Загрузка дистрибутивов

Также зайдите на своём компьютере в директорию dist и скачайте нужный архив

    cd dist
    wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu2204-6.0.5.tgz
    wget https://downloads.mongodb.com/compass/mongosh-1.8.0-linux-x64.tgz

Ссылки для скачивания можно найти на странице:

    https://www.mongodb.com/try/download/community-kubernetes-operator

Тут можно скачать community server.

## 1.2) Копирование дистрибутивов на сервера

Теперь скопируйте полученный архив на сервера установки:

    scp mongodb-linux-x86_64-ubuntu2204-6.0.5.tgz master1:dist/
    scp mongodb-linux-x86_64-ubuntu2204-6.0.5.tgz master2:dist/
    scp mongodb-linux-x86_64-ubuntu2204-6.0.5.tgz master3:dist/

    scp mongosh-1.8.0-linux-x64.tgz master1:dist/
    scp mongosh-1.8.0-linux-x64.tgz master2:dist/
    scp mongosh-1.8.0-linux-x64.tgz master3:dist/

## 1.4) Установка на один сервер

Далее зайдите на первый сервер:

    ssh master1

Дальнейшие действия нужно повторить для каждого сервера, где устанавливается кластер MongoDB.

Войдите в директорию dist и распакуйте архив:

    cd dist
    tar xvf mongodb-linux-x86_64-ubuntu2204-6.0.5.tgz
    tar xvf mongosh-1.8.0-linux-x64.tgz

Появятся директории: mongodb-linux-x86_64-ubuntu2204-6.0.5 и mongosh-1.8.0-linux-x64

Подготавливаем директории установки:

    sudo mkdir -p /opt/mongodb-6.0.5/bin
    sudo mkdir -p /opt/mongodb-6.0.5/conf
    sudo ln -s /opt/mongodb-6.0.5 /opt/mongodb

Устанавливаем исполняемые файлы:

    sudo cp mongodb-linux-x86_64-ubuntu2204-6.0.5/bin/mongod /opt/mongodb/bin/
    sudo cp mongodb-linux-x86_64-ubuntu2204-6.0.5/bin/mongos /opt/mongodb/bin/
    sudo cp mongosh-1.8.0-linux-x64/bin/mongosh /usr/local/bin/

Создаём пользователя для сервера базы данных:

    sudo useradd mongo

Подготовим директория для сервера:

    sudo mkdir -p /data/mongo/data
    sudo chown -Rv mongo:mongo /data/mongo

Ручной запуск сервера можно сделать выполнив команду:

    sudo -u mongo /opt/mongodb/bin/mongod --dbpath /data/mongo/data --bind_ip_all --replSet rs1

Теперь создадим файл системного сервиса:

    sudo mcedit -b /etc/systemd/system/mongo.service

И вставим туда текст:

    [Unit]
    Description=MongoDB Database Service
    Wants=network.target
    After=network.target
    
    [Service]
    Type=forking
    PIDFile=/data/mongo/mongo.pid
    ExecStart=/opt/mongodb/bin/mongod --dbpath /data/mongo/data --bind_ip_all --replSet rs1 --fork --syslog
    ExecReload=/bin/kill -HUP $MAINPID
    Restart=always
    User=mongo
    Group=mongo
    StandardOutput=syslog
    StandardError=syslog

    MemoryHigh=500M
    MemoryMax=500M
    
    [Install]
    WantedBy=multi-user.target

Обновим системные файлы, активируем сервис и запустим его:

    sudo systemctl daemon-reload
    sudo systemctl enable mongo
    sudo systemctl start  mongo

Теперь все эти действия нужно провести на всех серверах кластера MongoDB.

## 1.5) Настройка набора реплик

Теперь необходимо инициировать массив реплик на этих серверах. Для этого заходим на первый сервер, и запускаем команду:

    ssh master1
    mongosh

В провалившееся окно вставляем код инициализации:

    var cfg = {
      "_id": "rs1",
      "protocolVersion": 1,
      "members": [
        {
          "_id": 0,
          "host": "192.168.17.131:27017"
        },
        {
          "_id": 1,
          "host": "192.168.17.132:27017"
        },
        {
          "_id": 2,
          "host": "192.168.17.133:27017"
        }
      ]
    };
    rs.initiate(cfg, { force: true });
    rs.reconfig(cfg, { force: true });

В этой команде имеются IP-адреса: 192.168.17.131 - master1, 192.168.17.132 - master2, 192.168.17.133 - master3
Поменяйте их на нужные.

Проверить корректность инициализации, можно командой:

    rs.status();

Она должна вывести один сервер в состоянии PRIMARY, а две другие в состоянии SECONDARY.
Это происходит не сразу - надо немного подождать, когда сервера договорятся друг с другом.
Должно быть примерно так:
   
    members: [
        {
          _id: 0,
          name: '192.168.17.131:27017',
          health: 1,
          state: 1,
          stateStr: 'PRIMARY',
          ....
        },
        {
          _id: 1,
          name: '192.168.17.132:27017',
          health: 1,
          state: 2,
          stateStr: 'SECONDARY',
          ....
        },
        {
          _id: 2,
          name: '192.168.17.133:27017',
          health: 1,
          state: 2,
          stateStr: 'SECONDARY',
          ....
        }
    ],

Теперь можно пользоваться кластером.

Что бы к нему можно было присоединяться удалённо запустите в kubernetes yaml-файл:

    kubectl apply -f yaml-files/020-MongoDb/90-mongo-express.yaml

Предварительно откорректируйте в нём параметры доступа к MongoDB.

Продолжение [Развёртывание кластера Zookeeper](../Zookeeper_Kafka/021-Zoo1.md)
