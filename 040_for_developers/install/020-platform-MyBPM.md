# 1) Установка и настройка платформы MyBPM

Перед установкой платформы MyBPM необходимо выбрать топологию среды, где будет устанавливаться платформа.
Топология определяется следующими факторами:

1. Степень отказоустойчивости (как часто система находиться в рабочем состоянии и долго ли сбоит);
2. Производительные ресурсы (количество серверов, их мощность, наличие RAID-массивов).
3. Степень безопасности хранения данных (нужно ли данные шифровать или достаточно только шифровать передачу данных)

## 1.1) Степень отказоустойчивости

Степень отказоустойчивости определяется формулой, которая называется формулой отказоустойчивости на систему.

Формула отказоустойчивости на систему состоит из трёх индексов: D, H и А разделённых дефисом.

Индекс D определяет в какие дни недели необходимо обеспечить функционирование системы,
и может иметь следующие значения:

1. D7 - Постоянная работа, т.е. 7 дней в неделю.
2. D5 - Рабочие дни с понедельника по пятницу.

Индекс H определяет в какие часы дня необходимо обеспечить работу системы, и может иметь следующие значения:

1. H24 - Система работает полные сутки - 24 часа
2. H8 - Система работает в рабочее время, с 9:00 до 18:00

Индекс А определяет частоту простоев, и может иметь следующие значения:

1. A0 - Обеспечена работа **без простоев** (но ошибки возможны)
2. A5 - С вероятными простоями не больше **5 часов**
3. A24 - С вероятными простоями не больше **суток**

Например, индекс отказоустойчивости W5-D8-A5 обозначает работу системы в рабочее время с 9 до 18 с возможными простоями
не больше нескольких часов.

Самый жёсткий индекс: D7-H24-A0 - 24/7 без простоев.
Самый лёгкий индекс: D5-H8-A24 - рабочие дни в рабочее время с возможностью простоя до суток.

## 1.2) Производительные ресурсы

Производительные ресурсы зависят:

1) от количества пользователей системы;
2) от количества данных в системе;
3) от количества процессов работающих одновременно;
4) от индекса А в формуле отказоустойчивости.

## 1.3) Степень безопасности хранения данных

Есть две степени безопасности хранение данных:

1) Хранение на диске в открытом виде;
2) Шифрование диска.

## 1.4) Топология

### 1.4.1) Общие сведения о компонентах

Топология выбирается в зависимости от производительных ресурсов, индекса А в степени устойчивости и степени безопасности
хранения данных.

Платформа включает в себя следующие компоненты:

1) MyBPM Web
2) MyBPM API
3) Apache Kafka
4) Apache Zookeeper
5) MongoDB
6) ElasticSearch
7) PostgreSQL

Пользователи с помощью браузера подключаются к компоненту MyBPM Web (или просто Web-компонент), который отображает
пользователю графический интерфейс посредством динамического HTML и JavaScript. Web-компонент в свою очередь обращается
к компоненту MyBPM API (или просто API-компонент). API-компонент представляет собой state-less-приложение на основе
Boot Spring Framework. Он в свою очередь обращается одновременно к первым пяти компонентам.

Компонент Apache Kafka (или просто кафка) используется для хранения истории изменений в системе и как брокер сообщений.

Компонент Apache Zookeeper (или просто зукипер) используется для хранения конфигурации системы и для синхронизации
данных в Кафке.

Компонент MongoDB (или просто монго) используется для хранения оперативных данных, структуры данных и
реализации некоторых вспомогательных механизмов.

Компонент ElasticSearch (или просто эластик) используется для поиска данных.

Компонент PostgreSQL (или просто постгрес) используется для реализации различных вспомогательных механизмов.

### 1.4.2) Топология и установка зукипера и кафки

Имеются топологии зукипера:

1) Zoo1 - одна инстанция зукипера
   (см. [Установка Zookeeper с топологией Zoo1](Zookeeper_Kafka/021-Zoo1.md));
2) Zoo3 - три инстанции зукипера как один кластер
   (см. [Установка Zookeeper с топологией Zoo3](Zookeeper_Kafka/020-Zoo3.md)).

Имеются топологии кафки:

1) Kafka1-F1 - одна инстанция с фактором репликации один
   (см. [Установка Kafka по топологии Kafka1-F1](Zookeeper_Kafka/031-Kafka1-F1.md));
2) Kafka3-F2 - три инстанции как один кластер с фактором репликации два
   (см. [Установка Kafka по топологии Kafka3-F2](Zookeeper_Kafka/030-Kafka3-F2.md)).

### 1.4.3) Топология и установка монги

Имеются топологии монги:

1) Mongo1-RS1 - одна инстанция как одна группа репликации rs1
   (см. [Установка монги по топологии Mongo1-RS1](MongoDB/010-Mongo1-RS1.md))
2) Mongo3-RS1 - три инстанции как одна группа репликации rs1
   (см. [Установка монги по топологии Mongo3-RS1](MongoDB/020-Mongo3-RS1.md)).

### 1.4.4) Топология и установка эластика

Имеются топологии эластика:

1) Elastic1-F1 - одна инстанция с фактором репликации один
   [Установка эластика по топологии Elastic1-F1](Elastic/010-Elastic1-F1.md);
2) Elastic3-F2 - три инстанции как один кластер с фактором репликации два
   [Установка эластика по топологии Elastic3-F2](Elastic/020-Elastic3-F2.md)

### 1.4.4) Топология и установка постгреса

Имеются топологии постгреса:

1) PG1 - один сервер без репликации;
2) PG1-R1-MAN - один сервер-мастер и один сервер репликации с ручной активацией реплики;
3) PG1-R2-AUTO - один сервер-мастер и два сервера репликации с автоматической активацией реплики как мастера.

## 1.5) Топология и установка MyBPM API и Web

MyBPM API можно запускать в трёх режимах:

1) MyBPM-API-Consumer-mix - смешанный режим - он обрабатывает API-запросы и сообщения кафки.
2) MyBPM-API-сервиса - он отвечает за обработку API-запросов как со стороны MyBPM-Web так и сторонними системами;
3) MyBPM-Consumer-обработчик - он отвечает за обработку сообщений от кафки;

MyBPM-API-Consumer-mix обозначается буквой M
MyBPM-API-сервис обозначается буквой A
MyBPM-Consumer-обработчик обозначается буквой C

Формула топологии API и Web состоит из нескольких частей разделённых дефисом:

    [Основа]:MN-AN-CN-WN

Где N - это число от 0 и больше. Букву с нулём можно не писать, а можно писать. W - с нулём не бывает

[Основа] - это применяемая виртуализация. Может принимать одно из двух значений:

1) Do - Docker - приложение запускается на docker с использованием docker-compose;
2) Ku - Kubernetes - приложение запускается в кластере Kubernetes.

Например, формула: Do:M1-W1 - обозначает, что система поднимает одну инстанцию в смешанном режиме
(MyBPM-API-Consumer-mix) с помощью docker-compose на докере.

Например, формула: Ku:A2-C3-W2 - обозначает, что система разворачивается в кластере Kubernetes и запускает два
MyBPM-API-инстанции, три обработчика сообщений кафки (MyBPM-Consumer) и два веб-модуля MyBPM Web.

## 1.5) Выбор топологии по предоставленным серверам.

Платформу можно установить на следующих вариантах наборов серверов:

#### Вариант 1

Один сервер:
- CPU: 8 ядер Core i7+
- RAM: 32GB
- HDD: 500GB

Тут нужно выбирать формулу платформы: Do:M1-W1 (Zoo1, Kafka1-F1, Mongo1-RS1, Elastic1-F1, PG1)
Степень отказоустойчивости: от D5-H8-A24 до D7-H24-A5. Индекс A0 на одном сервере невозможен.

#### Вариант 2

Три сервера. Каждый минимум
- CPU: 8 ядер Core i7+
- RAM: 32GB
- HDD: 500GB

Тут нужно выбирать формулу платформы:

    Ku:M3-W2 (Zoo3, Kafka3-F2, Mongo3-RS1, Elastic3-F2, PG1-R1-MAN или PG1-R2-AUTO)

Степень отказоустойчивости:
- D5-H8-A24 - (PG1-R1-MAN)
- D7-H24-A0 - данная степень дороже (PG1-R2-AUTO)

#### Вариант 3

Три сервера для Zookeeper и Kafka
- CPU: 8 ядер Core i7+
- RAM: 16GB
- HDD: 500GB

Три сервера для MongoDB
- CPU: 8 ядер Core i7+
- RAM: 16GB
- HDD: 500GB

Три сервера для ElasticSearch
- CPU: 8 ядер Core i7+
- RAM: 16GB
- HDD: 500GB

Четыре сервера для PostgreSQL и MyBPM API и Web
- CPU: 4 ядер Core i7+
- RAM: 16GB
- HDD: 500GB

Тут выбирается формула платформы:

     Ku:M3-W2 (Zoo3, Kafka3-F2, Mongo3-RS1, Elastic3-F2, PG1-R2-AUTO)
