# IN Migration Kafka BoiInputFieldBo

## Условия для того чтобы инстанций записалась в систему, поле должно быть уникальным!!!

### Структура класса BoiInputFieldBo.java

    public String fieldCode;
    
    public String toBoCode;
    
    public String toFieldCode;
    
    public String apiValue;

---

    public String fieldCode 

**fieldCode** - код поля в которой должна записаться инстанция, равен ***BoFieldDto.code()***


---

    public String toBoCode

**toBoCode** - код Бизнес Объекта в которой должна записаться инстанций, равен ***BoDto.code()***

Используется для миграций СО, для определения в какой именно БО должна записаться инстанция

***Можно не определять если это БО***

---

    public String toFieldCode

**toFieldCode** - код поля вложенного Бо

---

    public String apiValue

**apiValue** - Значение из RestAPI, Примечание значения должно быть уникальное

Для работы с apiValue используется Интерфейс BoFieldTypeProvider.java

___

Директория где можете посмотреть код BoFieldTypeProvider.java

    kz/greetgo/mybpm/register/impl/bo/etc/BoFieldTypeProvider.java
