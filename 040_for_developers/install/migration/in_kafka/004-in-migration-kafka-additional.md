# IN Migration Kafka Additional

## Временное хранение смигрированных данных

Каждая мигрированная запись храниться в монго коллекций  
    
    mongo.mybpm_aux.KafkaMigrationRecord

Это предотвращает перезапись данных.

---

Класс, который используется в этой, коллекций:
    
    public class KafkaMigrationRecordDto

Расположения данного класса:

    kz/greetgo/mybpm/model_kafka_mongo/migration/KafkaMigrationRecordDto.java

---

## Schedulers of InMigrationKafka

Все шедулеры связанные с миграцией через кафку находиться в классе:

    public class InMigrationKafkaScheduler
---

Удаление временных записей из монго коллекций

        @Scheduled("#")
        @FromConfig("repeat every 5 hour")
        public void deleteRecords() {
        inMigrationKafka.deleteKafkaMigrationRecord();
        }
Повторяются каждые 5 часов, и удаляет просроченные ***KafkaMigrationRecordDto***. Время простроченных записей можно определить в конфиге

----
Запуск дополнительной миграций для не найденных значение ссылочных БО и СО

        @Scheduled("#")
        @FromConfig("repeat every 10 min")
        public void applyRefBoi() {
        inMigrationKafkaRefBoi.migrateFromTempTable();
        }

Запускается каждые 10 минут если находит значение то, смигрирует эту запись и сделает запись не актуальным во временной таблице. Примечание эти временные данные хранятся в ПОСТГРЕСЕ
___
Удаляет не актуальные записи в 23:30, под не актуальными записями имеется ввиду уже смигрированные данные которые описан выше

    @Scheduled("#")
    @FromConfig("23:30")
    public void deleteUnActualRecords() {
    inMigrationKafkaRefBoi.deleteUnActualRecords();
    }

---

## Config of InMigrationKafka
    
    @Description("Конфиг используемые во время миграции в через кафку")
    public interface InMigrationKafkaConfig {
    
    @Description("Количество часов до очищения временных записей")
    @DefaultIntValue(5)
    int recordCleanHours();
    
    @Description("Интервал в минутах для обновление не просаженных ссылочных записей")
    @DefaultIntValue(10)
    long interval();
    
    @Description("Количество минут до удаления старых записей из Базы")
    @DefaultIntValue(10080)
    long oldRecordDeleteMinutes();
    }

