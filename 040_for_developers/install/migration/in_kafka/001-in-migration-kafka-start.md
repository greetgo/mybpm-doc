# IN Migration Kafka Introduction

## Самое начало

    /kz/greetgo/mybpm/model_kafka_mongo/mongo/company/CompanyDto.java

Нужно заполнить с каких топиков загружать данные, ниже приведён пример.

<img src="../../../../010_for_users/010_pics/kafka_topic_list.png" width="100%" alt="kafka_topic_list">

## Какими данными работает миграция? 

Миграция работает данными в формате **JSON** класса **BoiInput.java**

---

Класс **BoiInput.java** предназначен для работы миграция через кафку

    kz/greetgo/mybpm/register/impl/migration/in_kafka/model/BoiInput.java

## Структура класса BoiInput.java 

    public String recordId;

    public String externalId;

    public String id;

    public String boCode;

    public List<BoiInputField> fields;

    public List<BoiInputFieldBo> boFields;

    public State state;


## Поля BoiInput.java для чего они

---
     public String recordId

**recordId** - Идентификатор данной входящей записи

Предназначен для предотвращения повторной записи данных, можно не определять, но не желательно. 

---
    public String externalId

**externalId** - Внешний идентификатор инстанций, равен ***BoiDto.externalId()***

Предназначен для определения инстанций **BoiDto.java**

---
    public String id

**Id** - Идентификатор инстанций, равен ***BoiDto.strId()***

Предназначен для определения инстанций **BoiDto.java**

---
    public String boCode

**boCode** - Код бизнес объекта в которой должна вставиться входящая запись, равен ***BoDto.code()***

---
    public List<BoiInputField> fields

**Fields** - список полей со значения которые участвует в миграций 

#### [Подробнее о Fields ](002-in-migration-kafka-fields.md)

---

    public List<BoiInputFieldBo> boFields;

**boFields** - список ссылочных полей со значения которые участвует в миграций


#### [Подробнее о BoFields ](003-in-migration-kafka-bo-fields.md)

---

    public State state

**state** - состояние инстанций (ACTUAL, REMOVED, ARCHIVED, TEST)
