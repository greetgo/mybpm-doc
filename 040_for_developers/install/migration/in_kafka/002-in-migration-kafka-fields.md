# IN Migration Kafka BoiInputField

## Структура класса BoiInputField.java

    public String code;
    
    public String apiValue;

---

    public String code 

**Code** - код поля в которой должна записаться инстанция, равен ***BoFieldDto.code()***


---

    public String apiValue

**apiValue** - Значение из RestAPI

Для работы с apiValue используется Интерфейс BoFieldTypeProvider.java

___

Директория где можете посмотреть код BoFieldTypeProvider.java

    kz/greetgo/mybpm/register/impl/bo/etc/BoFieldTypeProvider.java
