# IN Migration Start

## Самое начало

    kz/greetgo/mybpm/model_kafka_mongo/mongo/bo/field/BoFieldDto.java

Нужно проставить needLoadFromInTables=true в поля, которые должны быть мигрированы

## Генерация файла с DDL для формирования in_таблиц

    LaunchGenerateMigrationFile_TMP
    LaunchGenerateMigrationFile
    MigrationController

## Чтобы миграция запускалась нужно в конфиге

    /mybpm/configs/InMigrationPostgresConfig.txt

Поставить true у параметра hasInMigration

## Здесь находятся конфиги распределения сообщений кафки по топикам

    /mybpm/direct-configs/KafkaDistributionBoOverTopic.yaml

## Чтобы миграция запустилась нужно проставить код компании, в которой происходит миграция, в конфиге:

    /mybpm/configs/InMigrationConfig.txt

В параметре companyCode

## Загрузка тестовых данных в in_таблицы

    kz.greetgo.mybpm_util_light.probes.gen.InsertManyData

## Загрузка тестовых данных для массового теста миграции

    kz.greetgo.mybpm.register.impl.migration.in.InMigrationStarterImplTest.prepareDataForMigrationStart()

## Есть 2 метода запустить миграцию

    1. kz/greetgo/mybpm/register/controller/migration/MigrationController.java
    2. kz/greetgo/mybpm/register/scheduler/InMigrationScheduler.java

1. Через контроллер (только для теста)
2. Через scheduler (на проде нужно настраивать только через это)

## Конфиги нужные для миграции:

    1. kz/greetgo/mybpm/register/config/in_migration/InMigrationConfig.java
    2. kz/greetgo/mybpm/register/config/in_migration/InMigrationDataSourceConfig.java
    3. kz/greetgo/mybpm/register/config/in_migration/InMigrationPostgresConfig.java

## Классы, где смотреть код:
    1. kz/greetgo/mybpm/register/impl/migration/in/InMigrationStarterImpl.java
    2. kz/greetgo/mybpm/register/impl/migration/in/InMigrationImpl.java
    3. kz/greetgo/mybpm/register/consumer/controller/in_migration/InMigrationConsumer.java
    4. kz/greetgo/mybpm/register/impl/migration/in/InMigrationWorkerImpl.java
    5. kz/greetgo/mybpm/register/impl/migration/in/InMigrationKafkaRegisterImpl.java


