# IN Migration Additional Settings

## Где смотреть логи

    /var/log/mybpm/migration/in_migration.log

## Где смотреть трэйсы

    2. /var/log/mybpm/traces/inMigration.log

Чтобы увидеть трэйсы нужно будет в zoo navigator найти inMigrationTracer 
и в его **level** проставить TRACE вместо **OFF**

## Информативные таблицы миграции в базе aux1

###  1. in_migration.boi_reference

Таблица нужна для связок между полями типа BO или CO.
Она работает по логике:

"Если я по таблице связок получаю связанный объект, но его еще нет в системе, 
то он сохраняется в эту таблицу. Получается что каждый элемент который проходит миграцию, 
проверяет не ждет ли его кто-то из ранее прошедших миграцию"

### 2. in_migration.copy_table_info

Хранит информацию про **copy** таблицы. Нужен во избежания ошибок при создании **copy таблиц**

### 3. in_migration.errors

Хранит данные об ошибках при миграции, в частности нужен был для отлавливания неожиданных ошибок

### 4. in_migration.from_kafka_to_mongo_tracking

Хранит данные о том, сколько данных пришли в **InMigrationKafkaRegisterImpl.comeKafkaInMigration** 
и что с ними в итоге стало.

### 5. in_migration.log

Хранит данные об ошибках при миграции, в частности нужен был для отлавливания ожидаемых ошибок

### 6. in_migration.timings

Хранит данные про время выполнения самых долго работающих методов в **InMigrationKafkaRegisterImpl**

## Mongo DTO, которая связывает внешние in_id с внутренние boiId

    kz/greetgo/mybpm/model_kafka_mongo/mongo/bo/BoiInMigrationRefDto.java

## Логика очистки информативных таблиц находится в классе:

    kz/greetgo/mybpm/register/impl/migration/in/InMigrationCleanerImpl.java

Кроме **copy** таблиц. Copy таблицы создаются и удаляются в классе

    kz/greetgo/mybpm/register/impl/migration/in/InMigrationImpl.java
