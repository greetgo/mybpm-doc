# Мониторинг - Grafana dashboards

Kafka Exporter Overview - https://grafana.com/grafana/dashboards/7589-kafka-exporter-overview/

Kafka Metrics (JMX) - https://grafana.com/grafana/dashboards/11962-kafka-metrics/

Spring Boot Statistics - https://grafana.com/grafana/dashboards/6756-spring-boot-statistics/

JVM (Micrometer) - https://grafana.com/grafana/dashboards/4701-jvm-micrometer/
