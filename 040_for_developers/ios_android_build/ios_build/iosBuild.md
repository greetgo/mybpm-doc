# iOS build and uploading to App Store

## 1. Getting ready
1.1 Firstly, you need to update your git branch ( to get new changes ) and then write
<pre>npm install</pre>
After that, make sure that you prepared everything to **production**.

in environment.ts
<pre>
  ...
  production: true, 
  development: false,
  ...
</pre>

### Skip this step (1.2) if you did not add plugin

>1.2 If you added plugin, you need run command <pre>ionic cap sync ios --configuration=production </pre> in terminal
>
> After that close XCode, open ios/App (where located Podfile) directory in Finder, open terminal here and run command `pod install`

1.3 Now run command <pre> ionic cap copy ios --configuration=production </pre> in project's terminal

## 2. Xcode settings

2.1 Connect your device and build for testing. After, testing everything, you need to increase version.
Before archiving project check all settings in Xcode, like: general, signing and capabilities, info.plist and etc.

2.2 Click **Product -> Archive**. Wait a while!

2.3 After archiving new version, click **Distribute App -> App Store Connect** Next -> **Upload** Next -> Next -> **Automatically** Next -> Wait a while -> Click **Upload** -> Wait long :)

2.4 If app successfully uploaded click `Done`.

## 3. App Store Connect

3.1 Open **appstoreconnect.apple.com** -> Sign in -> My Apps -> Click **Your app** -> Open **TestFlight**

You will most likely see your application being processed. Wait a while.

3.2 After processing, click **App Store** and click **+** near apps for iOS. Write version for store.

3.3 Next you need to check app by credentials you provided to Apple. Write what's new in this version

Add new build to this version

Save it, then add to test.

That's it!


