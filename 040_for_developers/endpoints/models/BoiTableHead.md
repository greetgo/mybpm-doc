# Описание модели данных BoiTableHead

* **String** `fieldId`

    Идентификатор поля

* **String** `name`

    Наименование поля

* **int** `orderIndex`

    Позиция поля

* **[BoFieldArchetype](./BoFieldArchetype)** `boFieldArchetype`

    Тип поля

* **[BoNativeFieldType](./BoNativeFieldType)** `nativeFieldType`

    Тип нативного поля. Заполнено только тогда, когда _boFieldArchetype_ имеет значение _NATIVE_
