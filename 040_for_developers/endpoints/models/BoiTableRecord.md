# Описание модели данных BoiTableRecord

* **String** `instanceId`

    Идентификатор инстанции

* **[List](../Footnote#list)<[BoFieldValue](./BoFieldValue)>** `values`

    Список значений данной инстанции

* **[BoiState](./BoiState)** `state`

    Тип данной инстанции

* **boolean** `isTouched`

    Флаг, указывающий на то была ли прочитана данная инстанция или нет 
