# Как сгенерировать PDF файлы

Для начала нужно установить пакеты данной командой:

    sudo apt-get install pandoc texlive-latex-base texlive-fonts-recommended texlive-extra-utils texlive-latex-extra texlive-xetex

Далее запускаем скрипт [convertToPdfAndZip](convertToPdfAndZip.bash)
