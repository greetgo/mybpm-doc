#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 118

###
### sudo apt-get install pandoc texlive-latex-base texlive-fonts-recommended texlive-extra-utils texlive-latex-extra
### sudo apt-get install texlive-xetex
###

mkdir -p out

pandoc -V mainfont="Liberation Serif" -V monofont="Ubuntu Mono" --pdf-engine=xelatex -o out/Спецификация_CRUD.pdf 050-auth.d/*.md 101-crud.d/*.md
