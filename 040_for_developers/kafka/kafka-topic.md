## _*статья в стадии разработки_

# Kafka-топики

### Вводная часть

<p>
В этой статье описаны топики Кафка в mybpm. Общее описание, consumer-groups, рекомендации по retention-policy и т.д.
</p>

### Топик PERSON

#### Общее описание

В PERSON отправляются сообщения типа KafkaPerson при создании новых, изменении, архивации или удаления существующих Пользователей.

#### Consumer-groups

- boi__to__elastic__{version}
- person__ou_record_index_{version} - old
- business_instance__to__boi_event_{version}
- kanban__to__kanbanObserver
- out_migration_{version}
- operative_report_migration_{version}
- toPostgres__orgNode__{version}
- toPostgres__boi_author__{version}
- populate__total__search_{version}

#### Retention-policy

    - Ставить запрещено

### Топик COMPANY

#### Общее описание

В COMPANY отправляются сообщения при CRUD-операциях над следующими БО:
   - "Департамент" (KafkaDepartment), 
   - "Рабочая группа" (KafkaPersonGroup), 
   - "Аккаунт" (KafkaCompany), 
   - "Рабочее время" (KafkaWorkingTime)

#### Consumer-groups

- boi__to__elastic__{version}
- departmentOrPersonGroup__ou_record_index - old (только для KafkaDepartment и KafkaPersonGroup)
- business_instance__to__boi_event_{version}
- from_company__sendAdminInvitationLetters (только для KafkaCompany)
- kanban__to__kanbanObserver (игнорируется KafkaWorkingTime)
- out_migration_{version} - только KafkaDepartment
- operative_report_migration_{version}
- toPostgres__orgNode - только KafkaDepartment
- toPostgres__personGroup - только KafkaPersonGroup

#### Retention-policy

    - Ставить запрещено

### Топик BO

#### Общее описание

В БО попадают объекты типа KafkaBo, KafkaBoProcess, KafkaBoScriptVersions, KafkaBoScripts, KafkaAccessGroup.
- KafkaBo отправляется в topic при создании новых или обновлении существующих БО (именно БО, его структура, не инстанции).
- KafkaBoProcess отправляется в topic при создании новых или обновлении существующих БП (при изменении непосредственно самого процесса, не БО-владельца)
- KafkaBoScriptVersions отправляется в topic при первой обращении к скриптам (создание), при изменении комментария, при публикации/копировании/удалении версии скрипта
- KafkaBoScripts отправляется в topic при первом обращении к скриптам (создание), а также при изменении параметров запуска
- KafkaAccessGroup отправляется в topic при создании/изменении прав доступа БО

#### Consumer-groups

- kanban__to__kanbanObserver - только KafkaBo
- refresh_refs
- operative_report_bo_{version} - только KafkaBo
- report_mat_view_co_{version} - только KafkaBo

#### Retention-policy

    - Ставить запрещено

### Топик BO_GROUP

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Ставить запрещено

### Топик BOI

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Ставить запрещено

### Топик BOI_PROCESS

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Ставить запрещено

### Топик RUN

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Необходимо
    - 2-е суток (172800000 ms)

### Топик RUN_PROCESS

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Необходимо
    - 2-е суток (172800000 ms)

### Топик RUN_PROCESS_MANUAL_SAVE

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Необходимо
    - 2-е суток (172800000 ms)

### Топик MANUAL_REFRESH

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - На свое усмотрение

### Топик CHAT

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Запрещено

### Топик MAIN_MENU

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Запрещено

### Топик BO_ITEM_FILTER

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Запрещено

### Топик BOI_EVENT_PROCESS

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Не рекомендуется (только в случае переполнения памяти, конкретно этим топиком)

### Топик BOI_EVENT

#### Общее описание

#### Consumer-groups

#### Retention-policy

     - Не рекомендуется (только в случае переполнения памяти, конкретно этим топиком)

### Топик SEARCH_HINT

    Не реализовано

## Топик BOI_OPEN_COUNT

#### Общее описание

#### Consumer-groups

#### Retention-policy

     - На свое усмотрение

### Топик IN_MIGRATION

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Крайне рекомендуется
    - 7 дней (604800000 ms)

### Топик BRACKET_FILTER

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Запрещено

### Топик BRACKET_PERSON_FILTER

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Запрещено

### Топик REPORT

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Необходимо
    - 2-е суток (172800000 ms)

### Топик REPORT_GROUP

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Необходимо
    - 2-е суток (172800000 ms)

### Топик IN_MIGRATION_RUN_PROCESS

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Крайне рекомендуется
    - 2-е суток (172800000 ms)

### Топик BOI_DELETE

#### Общее описание

#### Consumer-groups

#### Retention-policy

    - Не рекомендуется
