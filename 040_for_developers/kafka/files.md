# Файлы

### 1. Скрипт, показывающий сколько физической памяти занимает каждый topic в Кафке

Выполнять данный файл нужно в контейнере Кафка, так как для работы необходимы скрипты kafka-topics.sh и kafka-log-dirs.sh.
Также для работы необходима утилита "jq"

[Скрипт](kafka-topics-size-new.sh). В файле {BOOTSTRAP_SERVER_ADDRESS:PORT} заменить на адрес bootstrap-server`а (по дефолту "localhost:9092")

### 2. Рекомендации по retention-policy (список)

[Список](topics-retention.txt) 
