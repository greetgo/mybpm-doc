# Интеграция через БД

<p>Функционал MyBPM для формирования структуры внешней Базы Данных SQL с автоматической выгрузкой и загрузкой данных.</p> 
Порядок действий:
<p> 1. В режиме редактирования, в необходимом Бизнес-объекте выбираем нужное вам поле для выгрузки.</p>
<p>2. Нажимаем на иконку настроек поля в виде шестеренки в углу поля.</p>
<p>3. В модальном окне в верхних вкладках выбираем пункт интеграций.</p>
<p>4. В разделе “Интеграция через БД” отмечаем необходимую опцию: </p>
- для загрузки из БД данных в MyBPM выбираем пункт “Загружать из IN_таблиц”,<br>
- для выгрузки данных из MyBPM в БД выбираем пункт “Выгружать в OUT_таблицу”.
<p>5. Сохраняем изменения в БО нажатием кнопки “Сохранить”.</p>

<img width="881" src="040_pics/settingUpInOut.png">

Настройка признака in/out

После всех настроек в MyBPM полей интеграции:

- Для загрузки данных в MyBPM формируется структура для SQL в формате DDL файла. Файл предоставляет разработчик MyBPM. Данный файл необходимо запустить в вашей внешней БД PostgreSQL, который создает всю структуру таблиц. Дальнейшее обновление данных должно соответствовать этой структуре. Предоставляются данные для подключения к БД.
- Для выгрузки данных из MyBPM тоже предоставляются данные для подключения к БД.

<b>!Важно</b>: При изменении полей для интеграции, формируется новая структура БД. Для того чтобы выгрузка произошла корректно, необходимо запустить описанный процесс заново.

Структура формируется по правилам:

| №   | Структура БО MyBPM            | Структура таблиц в БД            | Описание                                                                                                                                     |
|-----|-------------------------------|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Код бизнес-объекта            | Название таблицы с приставкой    |                                                                                                                                              |
| 2   | Код поля                      | Название колонки                 |                                                                                                                                              |
| 3   | Содержимое поля в MyBPM       | Значения поля                    ||
| 4   | Тип поля                      | Тип переменной                   | Ассоциирован с типами SQL согласно логике полей MyBPM                                                                                        |
| 5   | Вложенный БО                  | Внешний таблица со связанными id | Таблица содержит Id вложенного объекта, и Id принимающего                                                                                    |
| 6   | Вложенный Композитный объект  | Внешние таблицы со связанными id | Количество таблиц соответствует количеству вложенных БО в Композитный объект. Для каждого вложенного объекта формируется своя таблица связки | 

## Пример формирования структуры таблицы выгрузки IN из MyBPM.

<img width="881" src="040_pics/codeObject.png">

Код бизнес-объекта

<img width="881" src="040_pics/codeField.png">

Код поля
<pre>
В результате получаем таблицу с названием по коду БО и приставкой in.
create table public.in_Natural_client (
id bigserial primary key,
in_id varchar(100),
out_id varchar(32),
inserted_date timestamp with time zone default clock_timestamp(),
in_status varchar(100) default '' not null,
occupied_id bigint,
occupied_at timestamp with time zone,
removed bool default false,
IIN text,
Category varchar(32),
Date_Birth timestamp with time zone,
Date_death timestamp with time zone,
SUSN_type bool,
Relationship_status text,
Client_dep_colvir_ID numeric(24,4),
Gender varchar(32),
Amount_children numeric(24,4),
Client_code text,
Full_name text,
Global_client_ID numeric(24,4)
);
</pre>

Структура таблицы в описании:


| Имя колонки в базе | Имя колонки MyBPM    | Тип                   | Описание                                       |
|--------------------|----------------------|-----------------------|------------------------------------------------|
|                    | Global_client_ID     | int(8)                | Global ID клиента                              |
|                    | Category             | Справочник - Category | Категория                                      |
|                    | Full_name            | VARCHAR(256)          | ФИО                                            |
|                    | IIN                  | VARCHAR(256)          | ИИН                                            |
|                    | Identity             | Таблица - Identity    | Удостоверение                                  |
|                    | Date_Birth           | Date                  | Дата рождения                                  |
|                    | Date_death           | Date                  | Дата смерти                                    |
|                    | Relationship_status  | VARCHAR(256)          | Семейное положение                             |
|                    | Email                | Таблица - Email       | Электронная почта                              |
|                    | Gender               | Справочник - Gender   | Пол                                            |
|                    | Amount_children      | NUMERIC               | Кол-во детей                                   |
|                    | SUSN_type            | Bool                  | Тип СУСН                                       |
|                    | Client_dep_colvirID  | Numeric               | Идентификатор подразделения клиента в колвире  |

## Пример с вложенным БО

<img width="881" src="040_pics/codeNestedBO.png">

Код вложенного БО

Сформированная таблица с ключами на существующие таблицы, вложенные в БО.
Где  Natural_client_id ссылается на id в таблице in_Natural_client, то есть системный авто инкриминированный id, а  Place_work_id ссылается на   id в таблице in_ Place_work.

<pre>
create table public.in_Natural_client_Place_work (
    Natural_client_id varchar(100),
    Place_work_id varchar(100),
    in_status varchar(100) default '' not null,
    inserted_date timestamp with time zone default clock_timestamp(),
    primary key (Natural_client_id, Place_work_id)
);
</pre>

## Пример с вложенным Композитным объектом

<img width="881" src="040_pics/codeNestedCO.png">

Код вложенного КО

В данном случае вложенный КО ссылается на три БО - физические лица, юридические лица, ИП. Где Contracts_id ссылается на id таблицы in_Contracts, а Borrower_id на id в таблицах in_Natural_client, in_Legal_client, in_IP_client

<pre>
Таблица связка с таблицей физического лица.
create table public.in_Contracts_Borrower_Natural_client (
    Contracts_id varchar(100),
    Borrower_id varchar(100),
    in_status varchar(100) default '' not null,
    inserted_date timestamp with time zone default clock_timestamp(),
    primary key (Contracts_id, Billing_PR_Natural_client_id)
);
Таблица связка с таблицей юридического лица.
create table public.in_Contracts_Borrower_Legal_client (
    Contracts_id varchar(100),
    Borrower_id varchar(100),
    in_status varchar(100) default '' not null,
    inserted_date timestamp with time zone default clock_timestamp(),
    primary key (Contracts_id, Billing_PR_Natural_client_id)
);
Таблица связка с таблицей ИП.
create table public.in_Contracts_Borrower_IP_client (
    Contracts_id varchar(100),
    Borrower_id varchar(100),
    in_status varchar(100) default '' not null,
    inserted_date timestamp with time zone default clock_timestamp(),
    primary key (Contracts_id, Billing_PR_Natural_client_id)
);
</pre>

## Пример формирования OUT таблицы выгрузки данных из MyBPM

Для начала проверяется наличие схемы, если такая схема не существует, то создается по принципу:
- приставки out;
- кода компании в MyBPM;
- версии в коде сервера (code: OUT_MIGRATION_VERSION);
- версии в переменной окружения (yaml файл code:MYBPM_ OUT_MIGRATION_VERSION);
Пример: out_jusan_1_8;

<img width="881" src="040_pics/codeBO.png">

Код БО

<img width="881" src="040_pics/codeField2.png">

Код поля

Создается таблица с названием соответственно названию БО с кодами полей.
<pre>
create table Spouse_work_address (
    id bigserial primary key,
    system_created_at timestamp with time zone,
    system_creator_fio varchar(100),
    system_id varchar(100) unique,
    inserted_at timestamp with time zone default clock_timestamp(),
    updated_at timestamp with time zone,
    Street text,
    ATO_code text,
    Structure text,
    corps text,
    Microdistrict text,
    Locality text,
    Apartment text,
    House text,
    Index text
);
</pre>

Структура таблицы в описании:

| Имя колонки в DWH | Имя колонки MyBPM              | Тип                       | Описание          |
|-------------------|--------------------------------|---------------------------|-------------------|
|                   | Client_id (==Global_client_ID) | int(8)                    | Global ID клиента |
|                   | Address_type                   | Справочник - Address_type | Тип адреса        |
|                   | District                       | Справочник - District     | Район             |
|                   | City                           | Справочник - City         | Город             |
|                   | Region                         | Справочник - Region       | Область           |
|                   | Street                         | VARCHAR(256)              | Улица             |
|                   | House                          | VARCHAR(256)              | Дом               |
|                   | Apartment                      | VARCHAR(256)              | Квартира          |
|                   | ATO_code                       | VARCHAR(256)              | Код АТО           |
|                   | Locality                       | VARCHAR(256)              | Населенный пункт  |
|                   | Microdistrict                  | VARCHAR(256)              | Микрорайон        |
|                   | corps                          | VARCHAR(256)              | Корпус            |
|                   | Structure                      | VARCHAR(256)              | Строение          |
|                   | Index                          | VARCHAR(256)              | Индекс            |

## Системные поля

Поля являются системными:
- id bigserial primary key,
- system_created_at timestamp with time zone,
- system_creator_fio varchar(100),
- system_id varchar(100) unique,
- inserted_at timestamp with time zone default clock_timestamp(),
- updated_at timestamp with time zone.
