# 2022-11-18: Запускал отдельный кластер kafka

Yaml-файлы находятся в проекту mybpm-kube в папке probes/kubernetes-1.25.2-containerd-cilium-cluster

Разворачивался кластер на VirtualBox на моей рабочей машине. Сервера назывались:
    
- kube-server-c1
- kube-server-w1
- kube-server-w2
- kube-server-w3

В проекте mybpm-kube есть файл:

    probes/kubernetes-1.25.2-containerd-cilium-cluster/00-prepare/recreate-cluster.sh

Который нужно вызывать в файле:

    ~/.local/share/mybpm/handlers/recreate.sh

Вот содержание этого файла:

```bash
PRG=/home/pompei/IdeaProjects/mybpm.server/mybpm_debug_env
cd $PRG
docker-compose stop kafdrop kf zoo-navigator zoo

DIR=/home/pompei/IdeaProjects/mybpm-kube/probes/kubernetes-1.25.2-containerd-cilium-cluster/00-prepare

sh "$DIR/recreate-cluster.sh"
```
