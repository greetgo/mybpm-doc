# Возможности скриптов

## Вызов сервисов

Из скриптов можно вызывать внешние сервисы. Детально рассказано об этом в следующих видео:

- Вызов Rest-Json-сервиса: [https://youtu.be/kQ3SSDQOYsQ](https://youtu.be/kQ3SSDQOYsQ).
- Вызов XML-сервисов:
  - Общий подход: [https://youtu.be/muU0BHHMGCA](https://youtu.be/muU0BHHMGCA)
  - Чтение массива XML тегов: [https://youtu.be/3LWKQkACvMA](https://youtu.be/3LWKQkACvMA)
