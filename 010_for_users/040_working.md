# Рабочие группы

Отражает объединение пользователей как группы. У пользователя может быть несколько рабочих групп.

Поля БО включают:
- Имя - название рабочей группы, обязательное поле.
- Руководитель - пользователь, закрепленный за данной рабочей группы, имеет возможность на отдельные настройки прав доступа к системе.
- Пользователи - включенный множественный список пользователей из БО “Пользователи”, имеет взаимосвязь с БО пользователи.

<img width="881px" src="010_pics/PersonGroupForm.png">