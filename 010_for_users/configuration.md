# Конфигурации

Конфигурация — это набор заранее подготовленных бизнес-объектов ([Аналитика](060_report), [Департамент](030_department.md), [Пользователи](010_systemAdministration.md), [Компании](040_company), [Рабочие группы](040_working.md)).

Установка готовой конфигурации позволяет сэкономить время на ручное создание нужных бизнес-объектов. Все объекты, входящие в конфигурацию, после установки можно редактировать, создавать записи и т.п. - они ничем не отличаются по свойствам от объектов, созданных вручную.

