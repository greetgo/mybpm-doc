# Личный профиль пользователя

При нажатии на Имя пользователя откроется контекстное меню с пунктами:

- Перейти в профиль
- Сменить пароль
- Сменить язык
- Сведения о системе
- QR-код для мобильной версии
- Кнопка Выйти.

<img  src="010_pics/contextMenuPersonal.png">

## Профиль пользователя

По нажатию на кнопку “Перейти в профиль” в контекстном меню откроется мини карточка личного профиля со вкладками: Данные о пользователе и Безопасность.

<img width="100%" src="010_pics/personalUserProfile.png">

Во вкладке “Данные о сотруднике” содержится следующая информация: фамилия, имя, должность, телефон, e-mail, роль. (Пользователь может изменить информацию о себе).

_Примечание. При изменении Email, пользователю нужно будет подтвердить изменение, либо отменить. После подтверждения изменения Email, логином для входа в систему будет являться новый Email._

<img width="100%" src="010_pics/securityPassword.png">

## Сменить пароль

Сменить пароль - при нажатии откроется окно для ввода старого пароля и нового с дополнительным подтверждением. Пароль должен содержать не менее 8 символов. Могут использоваться следующие символы: цифры, буквы латиницы. Система проверяет соответствие нового пароля и пароля введенного для подтверждения - в случае несоответствия подсвечивает красным цветом, введены пароли не совпадают.

## Сменить язык

Сменить язык - при нажатии откроется окно для выбора языка.

<img width="100%" src="010_pics/languageSystems.png">

## Сведения о системе

Сведения о системе - при нажатии откроется окно данными по текущим версиям системы.

<img src="010_pics/systemVersion.png">

## QR-код для мобильной версии

QR-код для мобильной версии - при нажатии откроется окно с отображением QR - кода для сканирования при авторизации в мобильной версии системы.

<img src="010_pics/qrMobileVersion.png">

## Выйти

Выйти - при нажатии осуществляется выход из системы. Открывается окно для ввода логин/пароля для авторизации.











