# Описание возможностей

<hr>

## Базовые разделы и возможности

- [Личный профиль пользователя](personal_user_profile.md)
- [Структура компании](040_company)
- [Рабочие группы в MyBPM](040_working.md)
- [Аналитика](060_report)
- [Печатные формы](../020_forms_object/070_context_menu/020_types_context_menu/010_printed_forms.md)
- [Подписание с ЭЦП](../020_forms_object/020_widget/010_widget_EDS.md)
- [Скачивание и загрузка структуры](../010_for_users/dump.md)
- [Справочники]()
- [MyBPM для мобильных устройств]()
- [MyBPM на других языках]()

## Настройка и интеграция

- [Конфигурации](configuration.md)
- [Интеграции]()
- [Настройка форм](../020_forms_object.md)
- [Работа с полями](../020_forms_object/010_workWithField.md)
- [Права доступа](../020_forms_object/050_access_rights)
- [Виды отображения записей](../020_forms_object/060_setting_display_type.md)
- [Меню](../020_forms_object/071_Menu.md)
- [Композитные объекты](../020_forms_object/090_co.md)
- [Импорт данных](../020_forms_object/080_import.md)
- [Бизнес-процессы](../030_businessProcess_script)
- [Скрипты](../030_businessProcess_script/scripts.md)
- [API](../040_for_developers/api-service)
- [Отображение встроенных сайтов](../040_for_developers/potential_errors/web-iframe/general.md)
- [Плагины](../040_for_developers/plugin)
