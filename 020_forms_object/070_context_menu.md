# Контекстное меню

В каждом объекте (сущности) находятся подменю, в котором сгруппированы функции, которые можно применить ко всем объектам (сущностям).
Этот раздел называется контекстным меню.
На момент написания документа в контекстном меню присутствуют разделы:

- [Печатные формы;](070_context_menu/020_types_context_menu/010_printed_forms.md)

- [Участники;](070_context_menu/020_types_context_menu/020_members.md)

- [Чат;](070_context_menu/020_types_context_menu/030_chat.md)

- [Документы;](070_context_menu/020_types_context_menu/040_documents.md)

- [История изменения объекта (сущности)](070_context_menu/020_types_context_menu/050_history.md)
