# Настройка Бокового Меню

В Меню можно вывести основные объекты, которым необходим мгновенный доступ. По умолчанию, меню отображается в раскрытом виде, как показано на рисунке.

<img width="881px" src="030_pics/contractsSideMenu.png">

Меню

По мере необходимости, меню можно скрыть по нажатию на кнопку “<img width="35" src="030_pics/logoSideMenu.png">” в левом верхнем углу.

<img width="881px" src="030_pics/viewHiddenMenu.png">

Меню в скрытом виде

В скрытом виде меню, при наведении на иконку объекта, список объектов отобразиться в раскрытом виде. После перехода на нужный объект, меню скроется.

<img  src="030_pics/hoveringMenuHiddenForm.png">

Наведение на меню в скрытом виде

## Добавление нового меню

Для добавления объекты в меню, в режиме администрирования бизнес объектов нужно выбрать объект и с помощью drag&drop перетащить в меню.

<img width="881px" src="030_pics/addNewMenu.png">

Добавление нового меню

## Настройка местоположения меню

Для изменения местоположения объекта в меню, с помощью drag&drop нужно переместить в нужное место. Местоположение меню можно менять даже в режиме работы в системе.

<img width="881px" src="030_pics/settingMenuLocation.png">

Настройка местоположения меню

## Переименование меню

Для того чтобы переименовать меню, нужно выбрать объект, нажать на кнопку “⋮” рядом с наименованием меню, далее нажать кнопку “Переименовать”.

<img width="" src="030_pics/renameMenu.png">

Переименование меню

В поле “Изменение названия” нужно заполнить новое название меню, затем нажать сохранить.

<img width="881px" src="030_pics/newNameMenu.png">

Новое название меню

Примечание. Переименование меню будет отображено только в меню, наименование объекта будет прежним.

<img width="881px" src="030_pics/menuWithNewName.png">

## Настройка прав доступа на меню

Доступ на просмотр на меню можно разграничить как отдельным пользователям и департаментам, так и группам пользователей (ролям).

Для того чтобы настроить права, нужно нажать на кнопку “⋮” рядом с наименованием меню, затем нажать на кнопку “Права доступа”.

<img width="881px" src="030_pics/settingMenuLocation.png">

Настройка прав доступа на меню

В отобразившемся окне нужно переключить бегунок на “Выборочно”. Далее, нажать на кнопку “Выберите” и указать пользователей, департаменты или группы пользователи (роли), которые получат доступ на “Просмотр” данного меню.

<img width="881px" src="030_pics/addUsersDepartmentsForSettings.png">

Добавление пользователей/департаментов/групп пользователей для настройки прав доступа

Затем сохранить изменения по нажатию на кнопку “Сохранить”

## Настройка иконки

Для того чтобы настроить иконку в меню, нужно выбрать объект, нажать на кнопку “⋮” рядом с наименованием меню, далее нажать кнопку “Изменить иконку”.

<img width="881px" src="030_pics/settingsIcon.png">

Настройка иконки

Из предложенных вариантов иконок, нужно выбрать соответствующую иконку.

<img width="881px" src="030_pics/iconSelectionOptions.png">

Варианты выбора иконок

<img width="881px" src="030_pics/changingIconUrgentRequestsMenu.png">

Изменение иконки в меню “Срочные заявки”

## Настройка фильтра

В системе, по одному объекту можно добавлять несколько меню с разными фильтрами. В каждом из меню будут отображены записи в соответствии с фильтром, который указан в меню.

Для того чтобы добавить фильтр в меню, нужно выбрать объект, нажать на кнопку “⋮” рядом с наименованием меню, далее нажать кнопку “Фильтр”. Список полей (поля, которые выведены на карточку объекта, включая системные поля) для настройки фильтра, отобразиться по нажатию на кнопку “<img src="../010_Administration/010_pics/030_pics/plus.png">”.

<img width="881px" src="030_pics/settingFilter.png">

Настройка фильтра

Для примера, в меню “Срочные заявки” нужно настроить фильтр, где тип заказа = срочный. После сохранения изменений, в меню “Срочные заказы” будут отображены записи только с типом - срочные.

<img width="881px" src="030_pics/filterOrderTypeUrgent.png">

Фильтр тип заказа = срочный

<img width="881px" src="030_pics/displayingRecordsAccordingFilter.png">

## Настройка вида отображения записей

Для того чтобы настроить виды отображения записей, нужно выбрать меню, нажать на кнопку “⋮” рядом с наименованием меню. Далее из доступных видов отображения, выбрать вид отображения. Порядок видов отображения будет соответствовать порядку выбора отображения. 

<img width="881px" src="030_pics/settingUpDisplayViews.png">

Список настроенных видов отображения будет доступен справа в верхнем углу страницы.

<img width="" src="030_pics/listTypesDisplay.png">

## Удаление меню

Для того чтобы удалить меню, нужно нажать на кнопку “⋮” рядом с наименованием меню, затем нажать на кнопку “Удалить”.

<img width="881px" src="030_pics/deleteMenu.png">

Удаление меню

Далее, при нажатии “Да” меню будет удален, при нажатии на “Нет” действие по удалению объекта из меню будет отменен. 

<img width="881px" src="030_pics/actionConfirmationWindow.png">

Окно подтверждения действии

Примечание. При удалении меню, объект будет доступен в меню Бизнес

