# Настройка вида отображения

Для удобного визуального представления данных в объектах, в системе доступна настройка видов отображении:
- [реестр](../020_forms_object/060_setting_display_type/010_registry.md);
- [канбан](../020_forms_object/060_setting_display_type/020_canban.md);
- [календарь](../020_forms_object/060_setting_display_type/030_calendar.md);
- [карта](../020_forms_object/060_setting_display_type/040_map.md);
- [диаграмма Ганта](../020_forms_object/060_setting_display_type/).

