# Работа с полями

[Добавление поля в бизнес-объект](010_workWithField/010_addField.md)

[Удаления поля с бизнес-объекта](010_workWithField/020_deleteField.md)

[Расширенные настройки полей](010_workWithField/030_advancedSettingField.md)


