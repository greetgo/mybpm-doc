# Импорт 

Для массового добавления записей можно использовать Импорт данных. Клиентскую базу, список объектов, справочники или другие данные можно добавить в систему из файлов Excel за считанные минуты.

Импорт из Excel нужно выполнить перед началом работы в системе. Импорт также можно использовать для обновления и дополнения существующих записей.

В системе есть возможность импортировать данные сразу в 2 уровнях. Можно импортировать данные по залогам, и там же указать данные по оценке залога. После завершения импорта в 2 разделах (Залоги и оценка залога) появятся новые записи, а старые записи обновятся по уникальному полю. 

Кнопка для импорта находится в правом нижнем углу реестра в списочном виде. Кнопка доступна по всем имеющимся бизнес-объектам в системе. 

<img src="030_pics/ButtonsImport.png">

Пользователи, у которых есть права на экспорт на уровне бизнес-объекта могут импортировать файлы.

Рекомендации по подготовке файла для импорта. 

1.
Скачать шаблон файла для импорта. 

<img src="030_pics/TemplateDownload.png">

2. Проверить шапку на целостность нужных полей. 

3. При неполном количестве нужных полей для импорта, необходимо их добавить в шапку.

Если поле простое, необходимо добавить одно поле, и заполнить его названием из поля системы.
Если поле находится во вкладке, то путь к нему прописывается через точку.  Пример: Наименование Вкладки. Наименование Поля
Если поле из вложенного объекта, то необходимо заполнить на строку выше наименование вложенного объекта, и на второй строке название самого поле. В таком случае, необходимо также все простые поля объединить в две строки. 

4. Заполнить этот файл необходимыми данными.

<img src="030_pics/NewExcelFile.png">

Чтобы импортировать данные из книги Excel в систему:

- Нужно подготовить файл импорта в формате *.xlsx. Рекомендации по подготовке файла указаны выше.

- Запустить мастер импорта: нужно перейти в раздел, в который нужно импортировать данные, и нажать кнопку «Импорт из .xlsx». Таким образом, можно импортировать данные во всех объектах. 

- Указать путь к подготовленному файлу по нажатию на кнопку “Выберите файл”. 

- Нажать на кнопку «Загрузить».

- После откроется карточка с аналитикой загружаемых данных:

- Количество записей – количество записей имеющихся в файле.

- Новые – количество новых записей имеющихся в файле.

- Ошибок – количество ошибок по данным, к примеру, если не заполнено обязательное поле, то система выведет сообщения рядом с записью.

- Обновлений – количество обновляемых записей по уникальному полю. 

<img src="030_pics/ImportDataAnalytics.png">

- По нажатию на кнопку «Применить» будут загружены данные из файла. При необходимости можно отменить импорт по нажатию на кнопку «Отменить»

