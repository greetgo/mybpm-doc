# Панель

Панель это - элемент различных конструкций, на котором существует возможность расположения вложенных бизнес-объектов, а также блока iframe.

Для того чтобы добавить панель:

Нужно перейти в режим администрирования в меню “Бизнес” в левом верхнем углу страницы.

<img width="100%" src="020_pics/addPanel.png">

На открывшейся странице нужно нажать на кнопку “<img width="35" src="../020_forms_object/020_pics/plusBOBusiness.png">” и выбрать пункт “Панель”.

<img width="650" src="../020_forms_object/020_pics/ButtonToAddNewBO.png">

Откроется страница с заполненным наименованием панели, где номер соответствует количеству имеющихся панель в системе. В поле необходимо ввести наименование панели, которое будет соответствовать логике использования панели.

<img width="100%" src="020_pics/newPanel.png">

Далее, необходимо настроить макет страницы с помощью конструктора объектов. Здесь настраиваются разные элементы страницы, например, виджеты и вложенные объекты. Все настройки выполняются без использования кода с помощью простых инструментов.

Созданную панель можно переместить в боковое меню.

Пример работы с панелью:

<img src="020_pics/workWithPanel.gif">




