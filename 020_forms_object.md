# Формы и объекты

Объекты (сущности) используется для моделирования и управления бизнес-данными в системе. Договоры, Контакты, База знаний, Платежи, Мероприятия, Залоги и т. д. — все это объекты, которые содержат записи данных.

Вы можете иметь как готовые сущности, так и настраиваемые сущности (которые можно создавать в режиме администрирования).

<img width="100%" src="020_forms_object/020_pics/BusinessObjectConstructor.png">

Конструктор бизнес объектов в мастере

## Добавление объекта

Для того чтобы добавить новый объект:

Нужно перейти в режим администрирования по нажатию на кнопку “<img src="020_forms_object/020_pics/pencilBO.png">” в меню “Бизнес” в левом верхнем углу страницы.

<img width="100%" src="020_forms_object/020_pics/AdministrativeMode.png">

Режим администрирования

На открывшейся странице нужно нажать на кнопку “” и выбрать пункт “Бизнес объект”.

<img width="700px" src="020_forms_object/020_pics/ButtonToAddNewBO.png">

Кнопка добавления нового объекта

Откроется страница с заполненными наименованием объекта, где номер соответствует количеству имеющихся объектов в системе. В поле нужно ввести наименование объекта, которое будет соответствовать логике использования объекта.

<img width="100%" src="020_forms_object/020_pics/ClaimNewBusinessObject.png">

Новый объект “Заявки”

Далее, нужно настроить макет страницы записи с помощью конструктора объектов. Здесь настраиваются разные элементы страницы, например, разные типы полей, вкладки, виджеты и вложенные объекты. Все настройки выполняются без использования кода с помощью простых инструментов.

## Группирование объектов

Когда в системе имеются более 20 объектов (сущности), администрирование объектов усложняется. Для быстрого доступа к объектам, в системе имеется функция группирование объектов.

<img width="600px" height="790" src="020_forms_object/020_pics/BusinessObjectGrouping.png">

Для того чтобы добавить новую группу:

Нужно перейти в режим администрирования по нажатию на кнопку “<img src="020_forms_object/020_pics/ButtonToObjectAdmin.png">” в меню “Бизнес” в левом верхнем углу страницы.

<img width="100%" src="020_forms_object/020_pics/AdminPage.png">

В открывшейся странице нужно нажать на кнопку “<img src="020_forms_object/020_pics/ButtonToAdd.png">” и выбрать пункт “Группа”.

<img width="700px" src="020_forms_object/020_pics/ButtonToAddNewBO.png">

В списке групп, добавиться новая группа с заполненным наименованием группы, где номер соответствует количеству имеющихся групп в системе. В поле введите наименование группы, которое будет соответствовать логике использования группы, например, “Заявки”.

<img width="" src="020_forms_object/020_pics/ListOfGroup.png">

Наименование группы можно переименовать по мере необходимости. Для переименования группу, в режиме администрирования, нужно перейти к группе, нажать на кнопку “⋮” рядом наименование группы. При нажатии на кнопку “Переименовать”, появиться курсор в поле наименование группы, далее нужно указать наименование группы.

<img width="" src="020_forms_object/020_pics/RenameOfGroup.png">

Изменение расположения группы, с помощью drag&drop перенесите группу в нужное место.

<img width="" src="020_forms_object/020_pics/drapAndDropExample.gif">

Добавление нового объекта в группу. Для добавления нового объекта в группу, в режиме администрирования, нужно перейти к группе, нажать на кнопку “⋮” рядом наименование группы. При нажатии на кнопку “Добавить бизнес-объект”, откроется страница с заполненным наименованием объекта, где номер соответствует количеству имеющихся объектов в системе. В поле нужно ввести наименование объекта, которое будет соответствовать логике использования объекта, например, “Продукты”.

<img width="" src="020_forms_object/020_pics/exampleAddGroup.gif">


